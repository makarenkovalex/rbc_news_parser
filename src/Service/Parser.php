<?php

namespace App\Service;

use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;
use PHPHtmlParser\Dom;


class Parser
{
    const URL = 'https://www.rbc.ru/';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SingleNewsParser
     */
    private $singleNewsParser;

    public function __construct(EntityManagerInterface $entityManager, SingleNewsParser $singleNewsParser)
    {
        $this->entityManager = $entityManager;
        $this->singleNewsParser = $singleNewsParser;
    }

    public function parse()
    {
        $dom = new Dom();

        $dom->loadFromUrl(self::URL);
        /** @var Dom\Node\Collection $feed */
        $feed = $dom->find('.news-feed__item');

        $feed->each(function ($feed) {
            /** @var Dom\Node\HtmlNode $feed */
            $news = $this->singleNewsParser->parse($feed);
            if ($news !== null) {
                $this->entityManager->persist($news);
            }
        });

        $this->entityManager->flush();
    }
}
