<?php

namespace App\Controller;

use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/", name="news_list")
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function index(NewsRepository $newsRepository): Response
    {
        $news = $newsRepository->findAll();
        return $this->render('news/index.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @Route("/news/{id}", name="news_details")
     * @param NewsRepository $newsRepository
     * @param int $id
     * @return Response
     */
    public function newsDetails(NewsRepository $newsRepository, int $id): Response
    {
        $news = $newsRepository->find($id);
        return $this->render('news/newsDetails.html.twig', [
            'news' => $news,
        ]);
    }
}
